package auction;

/**<h1>Status</h1>
 * <p>This enum represents all the States that the class {@link auction.Auction Auction} can take.</p>
 * 
 * @author Daniel Jones, Mustafa Gench, Andrew Mooney
 *
 */

public enum Status {
	PENDING, BLOCKED, CLOSED, OPEN
}
