package auction;


/** 
 * <h1>Item</h1>
 * <p>This class represents an Item that can be specified by a seller and placed into an auction.</p>
 * @params ID The ID of the Item.
 * @params description Description of the item.
 * @params seller The Seller object that has created this Item.
 * 
 * @author Daniel Jones, Mustafa Gench, Andrew Mooney
 *
 */

public class Item {
	private int ID;
	private String description;
	private Seller seller;
	
	public Item(String description, Seller seller, int id) {
		this.description = description;
		this.seller = seller;
		this.ID = id;
	}
	
	public String getDescription() { 
		return this.description;
	}
	
	public int getID() {
		return this.ID;
	}
	
	public boolean checkSeller(Seller seller) {
		if (this.seller == seller) {
			return true;
		}
		return false;
	}
}
