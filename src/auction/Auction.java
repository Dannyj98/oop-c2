package auction;

import java.time.LocalDateTime;
import java.util.LinkedList;
import java.util.List;


/**<h1>Auction</h1>
 * <p>This class represents an auction and is used by the {@link system.sys} class.</p>
 * @param ID Id of the Auction.
 * @param startPrice The starting price for the auction.
 * @param reservePrice The reserve price for the auction.
 * @param closingDate The closing date for the auction. Specified as a LocalDateTime object.
 * @param status The current status of the auction.
 * @param item The Item being sold by this auction.
 * @param seller The Seller object that began this auction.
 * @param currentBidPrice The current highest price that has been placed on this auction.
 * 
 * @author Daniel Jones, Mustafa Gench, Andrew Mooney
 *
 */


public class Auction {
	private int id;
	private double startPrice;
	private double reservePrice;
	private LocalDateTime closingDate;
	private auction.Status status;
	private Item item;
	private Seller seller;
	private double currentBidPrice = 0.00;
	
	private List<Bid> bidList = new LinkedList<Bid>();
	
	
	public Auction (int id, Double startPrice, Double reservePrice, LocalDateTime closingDate, auction.Status status, Item item, Seller seller) {
		this.id = id;
		this.startPrice = startPrice;
		this.reservePrice = reservePrice;
		this.closingDate = closingDate;
		this.status = status;
		this.item = item;
		this.seller = seller;
	}
	
	public String getItemDescription() {
		return item.getDescription();
	}
	
	public Double getCurrentBid() {
		return this.currentBidPrice;
	}
	
	public Double getLowerIncrement() {
		return this.startPrice* 0.1;
	}
	
	public Double getUpperIncrement() {
		return this.startPrice* 0.2;
	}
	
	public String getSeller () {
		return this.seller.getUsername();
	}
	
	public LocalDateTime getClosingDate() {
		return this.closingDate;
	}
	
	public double getStartingPrice() {
		return this.startPrice;
	}
	
	public int getID() {
		return this.id;
	}
	
	public void close() {
		this.status = Status.CLOSED;
		
		Bid winner;
		// Check if someone has won
		try {
		// Winning Bid
			winner = bidList.get(bidList.size() - 1);

			if (this.currentBidPrice > this.reservePrice) {
				
				// Winner
				winner.getBuyer().victory(winner, this);
				seller.addMessage(String.format("Your Auction: %d, has been sold!", this.getID()));
				
			} else {
				// No one won
				seller.addMessage(String.format("Your Auction: %d, has not sold.", this.getID()));
			}
		} catch (Exception e) {
			// Alert the seller that no one bidded on the item
			seller.addMessage(String.format("Your Auction: %d, has not sold.", this.getID()));
		}
		//TODO: make red
		System.out.println("Auction closed");
		
	}
	
	public boolean placeBid(double newBid, Buyer buyer) {		
		// First Bid - check if its within limits, create new bid object and add to list
		if ((newBid >= (this.startPrice * 0.1)) && (newBid <= (this.startPrice * 0.2))) {
			bidList.add(new Bid(newBid, buyer));
			currentBidPrice = currentBidPrice + newBid;
			return true;
		}			
		return false;
	}
	
	public void verify() {
		this.status = Status.OPEN;
	}
	
	
	public boolean checkSeller(Seller a) {
		if (this.seller.equals(a)) {
			return true;
		} else {
			return false;
		}
	}
	
	public void setStatus(Status status) {
		this.status = status;
	}
	
	public Status getStatus() {
		return this.status;
	}
	
	public String getStatusString() {
		return this.status.toString();
	}
	
	public boolean isBlocked() {
		if (this.status == status.BLOCKED) {
			return true;
		}
		return false;
	}
	
	public void setBlocked() {
		this.status = status.BLOCKED;
	}
	
	public void unblock() {
		this.status = Status.OPEN;
	}
}
