package auction;

import java.time.LocalDateTime;
import java.util.List;

/**
 * <h1>Status Check</h1>
 * <p>This class implements the Runnable function and is designed to run in a seperate thread.</p>
 * <p>This class checks the status of each auction and closes an auction when it's 'closeingDate' has passed.</p>
 * @param List List containing auction objects.
 * @param Delay The desired delay that the check will run at.
 * 
 * @author Daniel Jones, Mustafa Gench, Andrew Mooney
 * @version 1.0
 * 
 */


public class StatusCheck implements Runnable{
	private List<Auction> auctions;
	private Integer delay; //In ms
	
	public StatusCheck(List<Auction> auction, Integer delay) {
		this.auctions = auction;
		this.delay = delay * 1000;
	}
	
	public synchronized void setDelay(Integer delay) {
		this.delay = delay * 1000;
	}
	
	@Override
	public void run() {
		while (true) {
			try {
				Thread.sleep(delay);
				synchronized (auctions) {
					// Check each auction if its close datetime is equal to the current time
					for (Auction a: auctions) {
						if (a.getStatus().equals(Status.OPEN)) {
							if (a.getClosingDate().isBefore(LocalDateTime.now())) {
								// Close auction
								a.close();
							}
						}
					}
				}
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
	}
}
