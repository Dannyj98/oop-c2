package auction;

import java.util.LinkedList;
import java.util.List;


/**{@inheritDoc}
 * 
 * <h1>Buyer</h1>
 * <p>This class extends the User class and is used to create Bids.</p>
 * 
 * @author Daniel Jones, Mustafa Gench, Andrew Mooney
 *
 */
public class Buyer extends User{
	
	private List<String> messages = new LinkedList<String>();
	
	public Buyer(String username, String password, int id) {
		super(username, password, id);
		// TODO Auto-generated constructor stub
	}
	
	public int getMessageTotal() {
		return this.messages.size();
	}
	
	public List getMessages() {
		return this.messages;
	}

	public void victory(Bid winner, Auction auction) {
		// Buyer has won an auction.
		messages.add("Congratulations! Your Bid on Item: "  + auction.getItemDescription() + " with amount: £" + winner.getAmount() + " has won!");
	}
}
