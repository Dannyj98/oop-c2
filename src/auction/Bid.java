package auction;



/**
 * 
 * <h1>Bid</h1>
 * <p>This class represents a Bid that can b place by a Buyer on an open auction.</p>
 * 
 * @params amount The amount that the buyer would like to bid.
 * @params buyer The Buyer object that has created this bid.
 * 
 * @author Daniel Jones, Mustafa Gench, Andrew Mooney
 *
 */
public class Bid {

	private double amount;
	private Buyer buyer;
	//private DateTime date;
	
	public Bid (double amount, Buyer buyer) {
		this.amount = amount;
		this.buyer = buyer;
		//this.date = date;
	}
	
	public double getAmount() {
		return this.amount;
	}
	
	public Buyer getBuyer() {
		return this.buyer;
	}
}
