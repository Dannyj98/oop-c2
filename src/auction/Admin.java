package auction;

/**{@inheritDoc}
 * 
 * <h1>Admin</h1>
 * <p>This class extends the User class and is used to create and store a Admin. An Admin can control and monitor the Auctions and Users</p>
 * 
 * @author Daniel Jones, Mustafa Gench, Andrew Mooney
 *
 */

public class Admin extends User{

	public Admin(String username, String password, int id) {
		super(username, password, id);
	}

}
