package auction;

import java.security.MessageDigest;

/**<h1>User</h1>
 * <p>This class contains the base of all users classes.</p>
 * 
 * @param ID new ID for the user.
 * @param Username The Username of this user
 * @param Password The Password of this user.
 * 
 * @author Daniel Jones, Mustafa Gench, Andrew Mooney
 *
 */

public class User {
	protected int ID;
	protected String username;
	protected String password;
	
	//TODO: Add encryption to password
	
	public User (String username, String password, int id) {
		this.ID = id;
		this.username = username;
		try {
			setPassword(password);
			
		} catch(Exception e) {
			
		}
		
	}
	
	private void setPassword(String password) {
		//TODO: Add encryption
		this.password = password;
	}
	
	public Boolean checkPassword(String pass) {
		
		if (password.equals(pass)) {
			return true;
		} else {
			return false;
		}
	}
	
	
	public String getUsername () {
		return this.username;
	}
	
	public int getID () {
		return this.ID;
	}
}
