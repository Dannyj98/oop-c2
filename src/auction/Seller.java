package auction;


/**{@inheritDoc}
 * 
 * <h1>Seller</h1>
 * <p>This class extends the User class and is used to create Auctions and Items. A Seller can control and monitor their Auctions.</p>
 * 
 * @author Daniel Jones, Mustafa Gench, Andrew Mooney
 *
 */

import java.util.LinkedList;
import java.util.List;

public class Seller extends User {

	private Boolean blocked = false;
	private List<String> messages = new LinkedList<String>();
	
	public Seller(String username, String password, int id) {
		super(username, password, id);
		// TODO Auto-generated constructor stub
	}
	
	public int getMessageTotal() {
		return this.messages.size();
	}
	
	public List getMessages() {
		return this.messages;
	}
	
	public Boolean isBlocked() {
		return this.blocked;
	}
	
	public void addMessage(String message) {
		messages.add(message);
	}
	
	public void setBlocked() {
		this.blocked = true;
	}
	
	public void unblock() {
		this.blocked = false;
	}

}
