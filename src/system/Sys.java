package system;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.Collections;
import java.util.LinkedList;
import java.util.List;
import java.util.Optional;
import java.util.Scanner;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

import auction.Admin;
import auction.Auction;
import auction.Buyer;
import auction.Item;
import auction.Seller;
import auction.Status;
import auction.StatusCheck;
import auction.User;

/**
 * <h1>Sys</h1>
 * <p>This is the main class which creates and stores all the other classes.</p>
 * @author Daniel Jones, Mustafa Gench, Andrew Mooney
 *
 */

public class Sys {
	private final Scanner S = new Scanner(System.in);
	private List<Auction> auctions = Collections.synchronizedList(new LinkedList<Auction>());
	private List<User> users = new LinkedList<User>();
	private List<Item> items = new LinkedList<Item>();

	//Initiate Buyer and Seller
	private Buyer buyer;
	private Seller seller;
	private Admin admin;

	DateTimeFormatter formatter = DateTimeFormatter.ofPattern("dd/MM/yyyy HH:mm");
	
	StatusCheck sc = new StatusCheck(auctions, 5);
	Thread t1 = new Thread(sc);

	public Sys() {
		//TODO: Add fake users.
		//Admin
		users.add(new Admin("DAN", "1234", users.size() + 1));
		//Sellers
		users.add(new Seller("ANDREW", "123", users.size() + 1));
		users.add(new Seller("MUSS", "234", users.size() + 1));
		
		//Buyers
		users.add(new Buyer("OLI", "234", users.size() + 1));
		users.add(new Buyer("SHAUN", "234", users.size() + 1));
		
		
		//Auctions
		auctions.add(new Auction(0, 10.00, 20.00, LocalDateTime.now().plusMinutes(1), Status.PENDING, new Item("Item 1",(Seller) users.get(1), items.size() + 1), (Seller) users.get(1)));
		auctions.add(new Auction(1, 10.00, 20.00, LocalDateTime.now().plusMinutes(2), Status.PENDING, new Item("Item 2", (Seller) users.get(2), items.size() + 1), (Seller) users.get(2)));
		auctions.add(new Auction(2, 10.00, 10.00, LocalDateTime.now().plusMinutes(1), Status.OPEN, new Item("Item 3",(Seller) users.get(2), items.size() + 1), (Seller) users.get(2)));
		
		
		//Bids
		auctions.get(2).placeBid(1.50, (Buyer) users.get(3));
		auctions.get(2).placeBid(1.51, (Buyer) users.get(4));
		auctions.get(2).placeBid(1.52, (Buyer) users.get(3));
		auctions.get(2).placeBid(1.53, (Buyer) users.get(4));
		auctions.get(2).placeBid(1.54, (Buyer) users.get(3));
		auctions.get(2).placeBid(1.55, (Buyer) users.get(4));
		auctions.get(2).placeBid(1.56, (Buyer) users.get(3));
		auctions.get(2).placeBid(2.00, (Buyer) users.get(4));

		
		t1.start();
		while (true) {
			mainMenu();
		}	
	}

	private void mainMenu() {		
		System.out.println("1. [L]ogin");
		System.out.println("2. [S]ign Up");
		System.out.println("3. [B]rowse Auctions");
		System.out.println("4. [E]xit");
		System.out.print("Pick: ");
		String answer = S.nextLine();
		
		switch (answer.toUpperCase()) {
		case "L":
			//Login
			login();
			break;
		case "S" :
			//Sign Up
			signUp();
			break;
		case "B" : 
			//Browse Auction
			browseAuctions();
			break;
		case "E" :
			System.exit(0);
			//Exit
			break;
		}
	}
	
	public void signUp() {		
		System.out.println("------ Create Account ------");
		System.out.println("What type of User would you like to be?");
		System.out.print("[S]eller / [B]uyer  : " );
		String accountType = S.nextLine().toUpperCase();
		
		System.out.print("Username: ");
		String username = S.nextLine().toUpperCase();
		
		System.out.print("Password: ");
		String password = S.nextLine();
		
		System.out.print("Confirm Password: ");
		String confirmPassword = S.nextLine();
		
		if (!password.equals(confirmPassword)) {
			System.out.println("Password do not match, please try again.");	
			return;
		}
		
		//Create new user Buyer/Seller and add to linked list
		if (isUsernameFree(username)) {
			
			if (accountType.equals("S")) {
				try {
					users.add(new Seller(username, password, users.size() + 1));
				} catch (Exception e ) {
					e.printStackTrace();
				}
			} else if (accountType.equals("B")) {
				try {
					users.add(new Buyer(username, password, users.size() + 1));
				} catch (Exception e ) {
					e.printStackTrace();
				}
			} else {
				System.out.println("Please enter eithe B for Buyer or S for Seller.");
				return;
			}
		} else {
			System.out.println("Username is already taken.");
			return;
		}
		
		System.out.println("Account Created");
	}
		
	private Boolean isUsernameFree(String username) {
		for(User a : users) {
			if (a.getUsername().equals(username)) {
				return false;
			}
			
		}
		return true;
	}
	
	private void login() {
		//Ask for credentials
		System.out.println("------ Login ------");
		System.out.print("Username: ");
		String username = S.nextLine().toUpperCase();
		
		System.out.print("Password: ");
		String password = S.nextLine();
		
		//users.stream().filter(o -> o.getUsername().equals(username)).filter(o -> o.checkPassword(password));
		
		
		if (username != null && password != null) {
			for(User u: users) {
				if (u.getUsername().equals(username)) {
					// Found user
					if (u.checkPassword(password)) {
						// Correct password
						if(Seller.class.isInstance(u)) {
							// User is a seller
							seller = (Seller) u;
							sellerMenu();
							return;
						} else if (Buyer.class.isInstance(u)) {
							// user is a buyer
							buyer = (Buyer) u;
							buyerMenu();
							return;
						} else {
							admin = (Admin) u;
							adminMenu();
							return;
						}
					}
				}
			}
		}
		System.out.println("Username or Password was incorrect.");
	}
	
	private void buyerMenu() {
		while (buyer != null) {
			System.out.printf("------ [Buyer Account] [%s]  ------%n", buyer.getUsername());
			System.out.println("1. [B]rowse Auctions");
			System.out.println("2. [P]lace Bid");
			System.out.printf("3. [M]essages - [%d] %n", buyer.getMessageTotal());
			System.out.println("4. [L]ogout");
			System.out.print("Pick: ");
			String answer = S.nextLine().toUpperCase();
			
			switch(answer) {
			case "B" :
				browseAuctions();
				break;
			case "P" :
				placeBid();
				break;
			case "M" :
				messages();
				break;
			case "L" :
				logout();
				break;
			}
		}
	}

	private void sellerMenu() {
		while (seller != null) {
			System.out.printf("------ [Seller Account] [%s]  ------%n", seller.getUsername());
			System.out.println("1. [S]tart Auction");
			System.out.println("2. [C]reate Item");
			System.out.println("3. [P]ending Auctions");
			System.out.printf("4. [M]essages - [%d] %n", seller.getMessageTotal());
			System.out.println("5. [L]ogout");
			System.out.print("Pick: ");
			String answer = S.nextLine().toUpperCase();
			
			switch(answer) {
			case "S" :
				startAuction();
				break;
			case "C" :
				createItem();
				break;
			case "P" :
				pendingAuctions();
				break;
			case "M" :
				messages();
				break;
			case "L" :
				logout();
				break;
			}
		}
	}
	
	private void adminMenu () {
		while (admin != null) {
			System.out.println("---------------------- Admin Menu ----------------------");
			System.out.println("1. Block a seller");
			System.out.println("2. Block an auction");
			System.out.println("3. Unblock a seller");
			System.out.println("4. Unblock an auction");
			System.out.println("5. Set Thread delay");
			System.out.println("6. Add new Admin account");
			System.out.println("7. Logout");
			String choice = S.nextLine();
			
			switch (choice){
			case "1" :
				System.out.println("+-----------+-------------------------+");
				System.out.println("| Seller ID | Seller Name             |");
				System.out.println("+-----------+-------------------------+");
				for (User s : users) {
					//System.out.println(s);
					if (s instanceof Seller && ((Seller) s).isBlocked() == false) {
						System.out.printf("| %-9d | %-23s | %n", s.getID(), s.getUsername());
					}
				}
				System.out.println("+-----------+-------------------------+");
				
			
				while (true) {
					int sellerID;
					System.out.print("ID of seller to block: ");
					try {
						sellerID = Integer.parseInt(S.next());
						Seller blockSeller = (Seller) users.stream().filter(o -> o.getID() == sellerID).filter(o -> ((Seller) o).isBlocked() == false).findFirst().orElse(null);
						if (blockSeller != null) {
							blockSeller.setBlocked();
							System.out.println("Blocked Seller: " + blockSeller.getUsername());
							break;
						} else {
							System.out.println("User not found. Please try again.");
						}
						
					} catch (Exception e) {
						System.out.println("Please enter an integer");
					}
				}
				break;
				
			case "2" :
				System.out.println("+------------+---------------------+---------------------------+------------+");
				System.out.println("| Auction ID | Seller              | Item Description          | Status     |");
				System.out.println("+------------+---------------------+---------------------------+------------+");
				
				for (Auction a : auctions) {
					if (a.getStatus() != Status.BLOCKED) {
						System.out.printf("| %-10d | %-19s | %-25s | %-10s |%n", a.getID(), a.getSeller(), a.getItemDescription(), a.getStatusString());
					}
				}
				
				System.out.println("+------------+---------------------+---------------------------+------------+");
				
				while (true) {
					System.out.print("Enter auction ID of auction to block: ");
					
					int auctionID;
					
					try {
						auctionID = Integer.parseInt(S.next());
						Auction blockAuction = (Auction) auctions.stream().filter(o -> o.getID() == auctionID).filter(o -> o.getStatus() != Status.BLOCKED).findFirst().orElse(null);
						if (blockAuction != null) {
							blockAuction.setBlocked();
							System.out.println("Blocked auction: " + blockAuction.getID());
							break;
						} else {
							System.out.println("Somehing");
						}
					} catch (Exception e) {
						System.out.println("Please enter an integer.");
					}
				}
				break;
				
			case "3" :
				System.out.println("+-----------+-------------------------+");
				System.out.println("| Seller ID | Seller Name             |");
				System.out.println("+-----------+-------------------------+");
				for (User s : users) {
					//System.out.println(s);
					if (s instanceof Seller && ((Seller) s).isBlocked() == true) {
						System.out.printf("| %-9d | %-23s | %n", s.getID(), s.getUsername());
					}
				}
				System.out.println("+-----------+-------------------------+");
				
			
				while (true) {
					int sellerID;
					System.out.print("ID of seller to unblock: ");
					try {
						sellerID = Integer.parseInt(S.next());
						Seller unblockSeller = (Seller) users.stream().filter(o -> o.getID() == sellerID).filter(o -> ((Seller) o).isBlocked() == true).findFirst().orElse(null);
						if (unblockSeller != null) {
							unblockSeller.unblock();
							System.out.println("Seller unblocked: " + unblockSeller.getUsername());
							break;
						} else {
							System.out.println("User not found. Please try again.");
						}
						
					} catch (Exception e) {
						System.out.println("Please enter an integer");
					}
				}
				break;
				
			case "4" :
				System.out.println("+------------+---------------------+---------------------------+------------+");
				System.out.println("| Auction ID | Seller              | Item Description          | Status     |");
				System.out.println("+------------+---------------------+---------------------------+------------+");
				
				for (Auction a : auctions) {
					if (a.getStatus() == Status.BLOCKED) {
						System.out.printf("| %-10d | %-19s | %-25s | %-10s |%n", a.getID(), a.getSeller(), a.getItemDescription(), a.getStatusString());
					}
				}
				
				System.out.println("+------------+---------------------+---------------------------+------------+");
				
				while (true) {
					System.out.print("Enter auction ID of auction to unblock: ");
					
					int auctionID;
					
					try {
						auctionID = Integer.parseInt(S.next());
						Auction blockAuction = (Auction) auctions.stream().filter(o -> o.getID() == auctionID).filter(o -> o.getStatus() == Status.BLOCKED).findFirst().orElse(null);
						if (blockAuction != null) {
							blockAuction.unblock();
							System.out.println("Unblocked auction: " + blockAuction.getID());
							break;
						} else {
							System.out.println("Somehing");
						}
					} catch (Exception e) {
						System.out.println("Please enter an integer.");
					}
				}
				break;
	
			case "5" :
				System.out.print("Enter new delay for thread in seconds: ");
				
				int newDelay;
				while (true) {
					try {
						newDelay = Integer.parseInt(S.next());
						//TODO: change delay in thread
						sc.setDelay(newDelay);
						break;
					}catch (Exception e) {
						System.out.println("Please enter an integer");
					}
				}
				
				break;
			
			case "6" :
				System.out.print("Username: ");
				String username = S.nextLine().toUpperCase();
				
				System.out.print("Password: ");
				String password = S.nextLine();
				
				System.out.print("Confirm Password: ");
				String confirmPassword = S.nextLine();
				
				if (!password.equals(confirmPassword)) {
					System.out.println("Password do not match, please try again.");	
					return;
				}
				
				//Create new user Buyer/Seller and add to linked list
				if (isUsernameFree(username)) {
					users.add(new Admin(username, password, users.size() + 1));
				} else {
					System.out.println("Username is already taken.");
					return;
				}
				break;
			case "7" :
				logout();
				break;
			}
		}

		
		
	}
	
	private void createItem() {
		System.out.println("-------------------- Create Item --------------------");
		System.out.print("Item name: ");
		String itemName = S.nextLine();
		items.add(new Item(itemName, seller, items.size() + 1));
		
		
	}

	private void logout() {
		// Set buyer & seller = null
		buyer = null;
		seller = null;
		admin = null;
	}

	private void messages() {
		List<String> messages;
		if (seller == null) {
			//Buyer
			messages = buyer.getMessages();
			
		} else {
			messages = seller.getMessages();
		}
		System.out.println("------------- Messages -------------");
		
		
		for(String a: messages) {
			
			System.out.printf("| %-20s %n", a);
		}
		
	}
	
	private void placeBid() {
		browseAuctions();
		System.out.println("Enter the auction ID of the auction you would like to bid on.");
		while (true) {
			System.out.print("Auction ID: ");
			int auctionToBidOn;
			try {
				auctionToBidOn = Integer.parseInt(S.next());
				
				
				try {
					Auction auctionBid = auctions.stream().filter(o -> o.getStatus().equals(Status.OPEN)).filter(o -> o.getID() == auctionToBidOn).findFirst().orElse(null);
					if (auctionBid != null) {
						System.out.printf("Lower Increment: £%.2f %n", auctionBid.getLowerIncrement());
						System.out.printf("Upper Increment: £%.2f %n", auctionBid.getUpperIncrement());
						//Auction exists
						double biddingPrice;
						while (true) {
							System.out.print("Bidding Price: £");
							try {	
								biddingPrice = Double.parseDouble(S.next());
								S.nextLine();
								break;
								// Add bid to auction
							} catch (NumberFormatException a) {
								System.out.println("Please enter a valid bidding price in the format: £xx.xx");
							}
						}
					
						if (auctionBid.placeBid(biddingPrice, buyer)) {
							System.out.println("Bid was added succesfully");
							break;
						}
						System.out.println("Bid failed. Please try again.");
						S.nextLine();
						
					}
				} catch (Exception e) {
					System.out.println("No such Auction with that ID.");
					S.nextLine();
				}
				
			} catch (Exception e){
				System.out.println("Please Enter an integer");
				S.nextLine();
			}	
		}		
	}
	
	private void printPendingAuctions() {
		if ((auctions.stream().filter(o -> o.getStatus() == Status.PENDING).filter(o -> o.checkSeller(seller) != false).findAny().orElse(null)) != null) {
				System.out.println("+------------+----------------------+-------------------+");
				System.out.println("| Auction ID | Item                 | Closing Date      |");
				System.out.println("+------------+----------------------+-------------------+");
				
				for (Auction a: auctions) {
					if (a.getStatus().equals(Status.PENDING) && a.checkSeller(seller)) {
						System.out.printf("| %-10d | %-21s| %-17s | %n", a.getID(), a.getItemDescription(), formatter.format(a.getClosingDate()));
					}
				}
				
				System.out.println("+------------+----------------------+-------------------+");
		} else {
			System.out.println("No Pending auctions. Please enter 'C' to exit.");
		}
	}
		
	private void pendingAuctions() {
		System.out.println("---------- Pending Auctions ----------");
		
		// Create list of all the user's auction ID's to make sure they can't verify someone else's
		List<Auction> usersAuctionID = new LinkedList<Auction>();
		
		for (Auction a : auctions) {
			if (a.checkSeller(seller)){
				usersAuctionID.add(a);
			}
		}
		
		do {
			printPendingAuctions();
			System.out.println("Enter Auction ID to verify or enter 'C' to exit");
			System.out.print("Enter ID of auction you want to verify: ");
			int auctionID;
			try {
				auctionID = Integer.parseInt(S.next());
				S.nextLine();
				
				try {
					Auction auc = auctions.stream().filter(o -> o.getID() == auctionID).filter(o -> o.checkSeller(seller) != false).findFirst().orElse(null);
					if (auc != null) {
						auc.verify();
						System.out.println("Auction has been approved and can now be found in 'Browse Auctions'");
					} else {
						System.out.println("Auction doesnt exist, please try again.");
					}
					
				} catch (Exception e){
					System.out.println("Auction with that ID does not exist");
				}
			} catch (NumberFormatException a) {
				System.out.println("Please enter an integer.");
			}
			S.nextLine();
		} while(!S.next().toUpperCase().equals("C"));
			

	}

	private void startAuction() {
		//TODO:	Change LocalDate to LocalDateTime and allow the user to enter a certain time of day.	
		
		if (seller.isBlocked()) {
			System.out.println("This seller has been blocked from starting auctions.");
			return;
		}
		
		System.out.println("1. [C]reate new item ");
		System.out.println("1. [U]se already defined item ");
		String itemChoice = S.nextLine().toUpperCase();
		Item auctionItem = null;
		switch (itemChoice) {
		case "C" : 		
			System.out.println("Please create a new Item to auction.");
			System.out.print("Item Description: ");
			String description = S.nextLine();
	
			auctionItem = new Item(description, seller, (items.size()+1));
			items.add(auctionItem);
			
			break;
		case "U" :
			System.out.println("+---------+------------------------------+");
			System.out.println("| Item ID | Item Description             |");
			System.out.println("+---------+------------------------------+");
			
			List<Integer> itemIDs= new LinkedList<Integer>();
			for (Item i : items) {
				if (i.checkSeller(seller)) {
					itemIDs.add(i.getID());
					System.out.format("| %-7d | %-28s | %n", i.getID(), i.getDescription());
				}
			}
			
			System.out.println("+---------+------------------------------+");
			
			while (true) {
				System.out.print("Please enter the Item ID of the Item you would like to sell: ");
				try {
					int itemID = Integer.parseInt(S.next());											
					auctionItem = items.stream().filter(o -> o.getID() == itemID).findFirst().orElse(null);
						
					if (auctionItem != null) {
						break;
					}
				} catch (Exception e) {
					System.out.println("Please enter an Item ID");	
				}
			}
		}
		

		
		System.out.println("Please enter auction details");
		
		// Ensures user enters a Double as a starting price.
		double startPrice;
		while (true) {
			System.out.print("Start Price: £");
			try {	
				startPrice = Double.parseDouble(S.nextLine());
				break;
			} catch (NumberFormatException a) {
				System.out.println("Please enter a valid starting price in the format: £xx.xx");
			}
		}
		
		// Ensures user enters a double as the reserve price.
		double reservePrice;
		while (true) {
			System.out.print("Reserve Price: £");
			try {	
				reservePrice = Double.parseDouble(S.nextLine());
				break;
			} catch (NumberFormatException a) {
				System.out.println("Please enter a valid reserve price in the format: £xx.xx");
			}
		}
		
		// Get closing Date
		LocalDateTime dateTime;
		while (true) {
			String closingDate;
			
			//LocalDateTime closingDate;
			try {
				System.out.println("Please enter the date and time you would like the auction to close in the format: DD/MM/YYYY HH:MM");
				System.out.print("Closing Date & Time: ");
				closingDate = S.nextLine();
				dateTime = LocalDateTime.parse(closingDate, formatter);
				
				// Check if date is later than current day and within 7 days
				
				if ((dateTime.isAfter(LocalDateTime.now())) && dateTime.isBefore(LocalDateTime.now().plusHours(168))) {
					break;
				} else {
					System.out.printf("You enetered an invalid date. Please enter a date before: %14s, and after: %14s", LocalDateTime.now().plusHours(168).toString(), LocalDateTime.now());
				}
				
				
			} catch (Exception e) {
				
				System.out.println("You entered the date in the incorrect format. Correct Format: DD/MM/YYYY HH:MM");
				System.out.println(e);
			}
		}
		
		//TODO:  Proper Error message for invalid date
		int auctionID = auctions.size();
		auctions.add(new Auction(auctionID, startPrice, reservePrice, dateTime, Status.PENDING, auctionItem, seller));
		System.out.println("Auction has been created and is now pending. Please Confirm the auction for it to begin");
	}

	public void placeAuction() {
		//TODO: Creates a new auction and add it to the array.
	}
	
	public void browseAuctions() {
		//TODO: Returns an array/list of all open auctions.
		System.out.println("------------------------------ Current Open Auctions ---------------------------------");
		System.out.println("+------------+----------------------+---------------+-------------+-------------------+");
		System.out.println("| Auction ID | Item                 |Starting Price | Current Bid | Closing Date      |");
		System.out.println("+------------+----------------------+---------------+-------------+-------------------+");

		if (!auctions.isEmpty()) {
			for (Auction a : auctions) {
				if (a.getStatus() == Status.OPEN) {
					System.out.printf("| %-10d | %-20s | £%-12.2f | %-11s | %-17s |%n", a.getID(), a.getItemDescription(), a.getStartingPrice(), (a.getCurrentBid() == null ? "No Bid" : "£" + a.getCurrentBid()), formatter.format(a.getClosingDate()));
				}
			}
		} else {
			System.out.println("+        No Auctions are open at the moment        +");
		}
		System.out.println("+------------+----------------------+---------------+-------------+-------------------+");
	}
}