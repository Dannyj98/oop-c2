package system;

/**
 * <h1>Entry</h1>
 * <p>This class is the entry point to the eAuction system.</p>
 * @author Daniel Jones, Mustafa Gench, Andrew Mooney
 *
 */
public class Entry {
	
	public static void main(String[] args) {
		Sys system = new Sys();
	}
}
